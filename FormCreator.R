setwd('/home/melatonin/Desktop/FormCreator')
source("Functions.R")

init("test.php")
setconfig('sql_table', '"Test"')
setconfig('sql_list', '"SELECT `ID`,`Textfield`, DATE_FORMAT(`Datefield`,\'%d.%m.%Y\') AS Datefield, TIME_FORMAT(`Timefield`, \'%H:%i\') AS Timefield FROM `Test` ORDER BY `ID` ASC "')
setconfig('sql_list_array', 'array("ID" => array("ID","10%"), "Textfield" => array("Text","30%"), "Datefield" => array("Date","30%"), "Timefield" => array("Time","30%"))')
setconfig('title', '"Einträge"')
setconfig('description', '"Hier eine Beschreibung"')
setconfig('header_add', '"Eintrag hinzufügen"');
setconfig('header_edit', '"Eintrag bearbeiten"');
setconfig('msg_add', '"Der Eintrag wurde angelegt!"');
setconfig('msg_edit', '"Der Eintrag wurde gespeichert!"');
setconfig('msg_delete', '"Die markierten Einträge wurden gelöscht!"');
setconfig('msg_noentries', '"Es wurden noch keine Einträge eingestellt"');
setconfig('button_savechanges', '"Änderungen speichern"')
setconfig('button_add', '"Hinzufügen"')
setconfig('button_delete', '"Markierte Einträge Löschen"')
setconfig('delete_dialog_title', '"Einträge löschen"')
setconfig('delete_dialog_content', '"Möchten Sie die markierten Einträge wirklich löschen?"')
setconfig('delete_dialog_yes', '"Ja"')
setconfig('delete_dialog_no', '"Nein"')
setconfig('error_occured', '"Es ist ein Fehler aufgetreten!"')

#Text(column="Textfield", label="LabelText", width="250px", width_td_left="40%", width_td_right="60%", options="")
#Textarea(column="Textfield", label="LabelText", width="250px", height="100px", width_td_left="40%", width_td_right="60%", options="")
#Select(column="Selectfield", label="LabelSelectfield", pleasechoose="true", array=list('value1'='label1','value2'='label2'), sql = "SELECT `ID` AS `Value`, `Value` AS `Label` FROM `Test2` ORDER BY `ID`", width="250px", width_td_left="40%", width_td_right="60%")
#Radio(column="Radiofield", label="LabelRadiofield", byrow="true", array=list('value1'='label1','value2'='label2'), sql = "SELECT `ID` AS `Value`, `Value` AS `Label` FROM `Test2` ORDER BY `ID`", width_td_left="40%", width_td_right="60%")
#Checkbox(label="LabelCheckbox", array=list('Checkbox1'='a', 'Checkbox2'='b'), sql="SELECT `ID` AS `Column`, `Value` AS `Value` FROM `Test2` ORDER BY `ID`", byrow="true", width_td_left="40%", width_td_right="60%", options="")

#Number(column="Numberfield", label="LabelNumber", width="auto", width_td_left="40%", width_td_right="60%", options="")
#Date(column="Datefield", label="LabelDate", width="auto", width_td_left="40%", width_td_right="60%", options="")
#Time(column="Timefield", label="LabelTime", width="auto", width_td_left="40%", width_td_right="60%", options="")
#Email(column="Emailfield", label="Emailfield", width="auto", width_td_left="40%", width_td_right="60%", options="")
#Url(column="Urlfield", label="Urlfield", width="auto", width_td_left="40%", width_td_right="60%", options="")
#File(column="Filefield2", label="Labelfield", filetypes=c('jpg','jpeg','png','gif'), maxfiles=1, description = "Bitte wählen Sie eine Datei aus oder schieben Sie eine Datei in dieses Feld! Erlaubt sind folgende Formate: \".implode(\", \",$filetypes).\".", width="auto", width_td_left="40%", width_td_right="60%", options="")

compile()
