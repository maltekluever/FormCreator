# INITIALISE VARIABLES
init <- function(phpfile){
  vars = c("block_checkingfields","block_processuploaded","block_inputadd","block_scriptfilesadd","block_inputedit","block_scriptfilesedit")
  for(i in vars){
    assign(i, c(), envir = .GlobalEnv)
  }
  assign("phpfile",phpfile,envir=.GlobalEnv)
  # init config
  assign("config",list(),envir=.GlobalEnv)
  setconfig("phpfile",paste("'",phpfile,"'",sep="",collapse=""))
}

# SET CONFIG-VARIABLES
setconfig <- function(variable, value){
  config <- get("config", envir=.GlobalEnv)
  config[variable] = value
  assign("config", config, envir=.GlobalEnv)
}

# GET CONFIG-VARIABLES
getconfig <- function(){
  keys = names(config)
  result = c();
  for(key in keys){
    result = c(result, paste(c("$",key," = ",config[key],""),sep="\n",collapse=""))
  }
  return(paste(result,";",sep="",collapse="\n"))
}

# APPENDING VARIABLES
append <- function(var,value,env=.GlobalEnv){
  get <- get(var, envir=.GlobalEnv)
  new <- c(get,value)
  assign(var, new, envir = .GlobalEnv)
}

# CONVERT LIST TO ASSOCIATIVE ARRAY (IN PHP)
list2arr <- function(list){
  keys = names(list)
  result = c();
  for(key in keys){
    result = c(result, paste(c("'",key,"'=>'",list[key],"'"),sep=", ",collapse=""))
  }
  result = paste('array(',paste(result,sep="",collapse=", "),')',sep="",collapse="")
  return(result)
}

# CONVERT VECTOR TO ASSOCIATIVE ARRAY (IN PHP)
vector2arr <- function(vector){
  if(length(vector)>0){
    return(paste("'",vector,"'",sep="",collapse=","))
  }else{
    return("");
  }
}


# SPLITS THE SOURCE CODE INTO A LIST, SEPARATED BY THE BLOCKS
getModules <- function(file="template.php"){
  conn <- file(file,encoding="UTF-8",open="r")
  line <-readLines(conn)
  result = list()
  temp = c()
  add=1
  for (i in 1:length(line)){
    if(length(grep('#### BLOCK:',line[i]))>0 && add==1){
      add=0
      result = c(result,list(temp))
      temp = c()
    }else if(add==0 && length(grep('#### BLOCK:',line[i]))>0){
      add=1
    }else if(add==1){
      temp = c(temp, line[i])
    }
  }
  result = c(result,list(temp))
  temp = c()
  close(conn)
  return(result)
}

# GET A SPECIFIC BLOCK
getBlock <- function(block,config="",file="template.php"){
  conn <- file(file,encoding="UTF-8",open="r")
  line <-readLines(conn)
  startline = 0
  endline = 0
  for (i in 1:length(line)){
    if(startline!=0 & endline==0 & trimws(line[i])==""){
      endline = i
    }
    if(trimws(line[i])==block){
      startline = i
    }
    
  }
  close(conn)
  # inject the config
  if(config!=""){ 
    whitespaces = gsub(block,"",line[startline])
    line[startline+1] = paste(whitespaces,config,sep="",collapse="");
  } 
  return(line[startline:endline])
}

# TEXT
Text <- function(column, label, width="250px", width_td_left="40%", width_td_right="60%", options=""){
  config = paste('$column = "',column,'"; $label ="',label,'"; $width="',width,'"; $width_td_left="',width_td_left,'"; $width_td_right="',width_td_right,'"; $options = "',options,'";',sep="",collapse="")
  append("block_checkingfields", getBlock("// CHECK TEXT", config))
  append("block_inputadd", getBlock("<!-- // ADD TEXT -->", paste('<? ',config,' ?>',sep="",collapse="")))
  append("block_inputedit", getBlock("<!-- // EDIT TEXT -->", paste('<? ',config,' ?>',sep="",collapse="")))
}

# TEXTAREA
Textarea <- function(column, label, width="250px", height="100px", width_td_left="40%", width_td_right="60%", options=""){
  config = paste('$column = "',column,'"; $label ="',label,'"; $width="',width,'"; $height="',height,'"; $width_td_left="',width_td_left,'"; $width_td_right="',width_td_right,'"; $options = "',options,'";',sep="",collapse="")
  append("block_checkingfields", getBlock("// CHECK TEXTAREA", config))
  append("block_inputadd", getBlock("<!-- // ADD TEXTAREA -->", paste('<? ',config,' ?>',sep="",collapse="")))
  append("block_inputedit", getBlock("<!-- // EDIT TEXTAREA -->", paste('<? ',config,' ?>',sep="",collapse="")))
}

# SELECT
Select <- function(column, label, width="250px", pleasechoose="true", array=list(), sql="", width_td_left="40%", width_td_right="60%", options=""){
  config = paste('$column = "',column,'"; $label ="',label,'"; $width="',width,'"; $pleasechoose=',pleasechoose,'; $array=',list2arr(array),'; $sql="',sql,'"; $width_td_left="',width_td_left,'"; $width_td_right="',width_td_right,'"; $options = "',options,'";',sep="",collapse="")
  append("block_checkingfields", getBlock("// CHECK SELECT", config))
  append("block_inputadd", getBlock("<!-- // ADD SELECT -->", paste('<? ',config,' ?>',sep="",collapse="")))
  append("block_inputedit", getBlock("<!-- // EDIT SELECT -->", paste('<? ',config,' ?>',sep="",collapse="")))
}

# RADIO
Radio <- function(column, label, byrow="false", array=list(), sql="",  width_td_left="40%", width_td_right="60%", options=""){
  config = paste('$column = "',column,'"; $label ="',label,'"; $byrow=',byrow,'; $array=',list2arr(array),'; $sql="',sql,'";  $width_td_left="',width_td_left,'"; $width_td_right="',width_td_right,'"; $options = "',options,'";',sep="",collapse="")
  append("block_checkingfields", getBlock("// CHECK RADIO", config))
  append("block_inputadd", getBlock("<!-- // ADD RADIO -->", paste('<? ',config,' ?>',sep="",collapse="")))
  append("block_inputedit", getBlock("<!-- // EDIT RADIO -->", paste('<? ',config,' ?>',sep="",collapse="")))
}

# CHECKBOX
Checkbox <- function(label, array=list(), sql="", byrow="true", width_td_left="40%", width_td_right="60%", options=""){
  config = paste('$label = "',label,'"; $array =',list2arr(array),'; $sql="',sql,'"; $byrow=',byrow,'; $width_td_left="',width_td_left,'"; $width_td_right="',width_td_right,'"; $options = "',options,'";',sep="",collapse="")
  append("block_checkingfields", getBlock("// CHECK CHECKBOX", config))
  append("block_inputadd", getBlock("<!-- // ADD CHECKBOX -->", paste('<? ',config,' ?>',sep="",collapse="")))
  append("block_inputedit", getBlock("<!-- // EDIT CHECKBOX -->", paste('<? ',config,' ?>',sep="",collapse="")))
}

# NUMBER
Number <- function(column, label, width="auto", width_td_left="40%", width_td_right="60%", options=""){
  config = paste('$column = "',column,'"; $label = "',label,'"; $width="',width,'"; $width_td_left="',width_td_left,'"; $width_td_right="',width_td_right,'"; $options = "',options,'";',sep="",collapse="")
  append("block_checkingfields", getBlock("// CHECK NUMBER", config))
  append("block_inputadd", getBlock("<!-- // ADD NUMBER -->", paste('<? ',config,' ?>',sep="",collapse="")))
  append("block_inputedit", getBlock("<!-- // EDIT NUMBER -->", paste('<? ',config,' ?>',sep="",collapse="")))
}

# DATE
Date <- function(column, label, width="auto", width_td_left="40%", width_td_right="60%", options=""){
  config = paste('$column = "',column,'"; $label = "',label,'"; $width="',width,'"; $width_td_left="',width_td_left,'"; $width_td_right="',width_td_right,'"; $options = "',options,'";',sep="",collapse="")
  append("block_checkingfields", getBlock("// CHECK DATE", config))
  append("block_inputadd", getBlock("<!-- // ADD DATE -->", paste('<? ',config,' ?>',sep="",collapse="")))
  append("block_inputedit", getBlock("<!-- // EDIT DATE -->", paste('<? ',config,' ?>',sep="",collapse="")))
}

# DATE
Time <- function(column, label, width="auto", width_td_left="40%", width_td_right="60%", options=""){
  config = paste('$column = "',column,'"; $label = "',label,'"; $width="',width,'"; $width_td_left="',width_td_left,'"; $width_td_right="',width_td_right,'"; $options = "',options,'";',sep="",collapse="")
  append("block_checkingfields", getBlock("// CHECK TIME", config))
  append("block_inputadd", getBlock("<!-- // ADD TIME -->", paste('<? ',config,' ?>',sep="",collapse="")))
  append("block_inputedit", getBlock("<!-- // EDIT TIME -->", paste('<? ',config,' ?>',sep="",collapse="")))
}

# DATE
Email <- function(column, label, width="auto", width_td_left="40%", width_td_right="60%", options=""){
  config = paste('$column = "',column,'"; $label = "',label,'"; $width="',width,'"; $width_td_left="',width_td_left,'"; $width_td_right="',width_td_right,'"; $options = "',options,'";',sep="",collapse="")
  append("block_checkingfields", getBlock("// CHECK EMAIL", config))
  append("block_inputadd", getBlock("<!-- // ADD EMAIL -->", paste('<? ',config,' ?>',sep="",collapse="")))
  append("block_inputedit", getBlock("<!-- // EDIT EMAIL -->", paste('<? ',config,' ?>',sep="",collapse="")))
}

# DATE
Url <- function(column, label, width="auto", width_td_left="40%", width_td_right="60%", options=""){
  config = paste('$column = "',column,'"; $label = "',label,'"; $width="',width,'"; $width_td_left="',width_td_left,'"; $width_td_right="',width_td_right,'"; $options = "',options,'";',sep="",collapse="")
  append("block_checkingfields", getBlock("// CHECK URL", config))
  append("block_inputadd", getBlock("<!-- // ADD URL -->", paste('<? ',config,' ?>',sep="",collapse="")))
  append("block_inputedit", getBlock("<!-- // EDIT URL -->", paste('<? ',config,' ?>',sep="",collapse="")))
}

# FILE
File <- function(column, label, filetypes=c(), maxfiles=1, description = "Bitte wählen Sie eine Datei aus oder schieben Sie eine Datei in dieses Feld! Erlaubt sind folgende Formate: \".implode(\", \",$filetypes)", width="auto", width_td_left="40%", width_td_right="60%", options=""){
  config = paste('$column = "',column,'"; $label = "',label,'"; $filetypes= array(',vector2arr(filetypes),'); $maxfiles=',maxfiles,'; $description = "',description,'"; $width="',width,'"; $width_td_left="',width_td_left,'"; $width_td_right="',width_td_right,'"; $options = "',options,'";',sep="",collapse="")
  append("block_checkingfields", getBlock("// CHECK FILE", config))
  append("block_inputadd", getBlock("<!-- // ADD FILE -->", paste('<? ',config,' ?>',sep="",collapse="")))
  append("block_inputedit", getBlock("<!-- // EDIT FILE -->", paste('<? ',config,' ?>',sep="",collapse="")))
  append("block_processuploaded", getBlock("// HANDLE UPLOADED FILES",config))
  append("block_scriptfilesadd", getBlock("<!-- // ADD SCRIPT FILE -->", paste('<? ',config,' ?>',sep="",collapse="")))
  append("block_scriptfilesedit", getBlock("<!-- // EDIT SCRIPT FILE -->", paste('<? ',config,' ?>',sep="",collapse="")))
}

compile <- function(){
  # merge modules and blocks
  modules = getModules()
  script = paste(
    paste(modules[[1]],collapse="\n"),"\n",
    getconfig(),"\n",
    paste(modules[[2]],collapse="\n"),"\n",
    paste(get("block_checkingfields", envir=.GlobalEnv),sep="",collapse="\n"),
    paste(modules[[3]],collapse="\n"),"\n",
    paste(get("block_processuploaded", envir=.GlobalEnv),sep="",collapse="\n"),
    paste(modules[[4]],collapse="\n"),"\n",
    paste(get("block_inputadd", envir=.GlobalEnv),sep="",collapse="\n"),
    paste(modules[[5]],collapse="\n"),"\n",
    paste(get("block_scriptfilesadd", envir=.GlobalEnv),sep="",collapse="\n"),
    paste(modules[[6]],collapse="\n"),"\n",
    paste(get("block_inputedit", envir=.GlobalEnv),sep="",collapse="\n"),
    paste(modules[[7]],collapse="\n"),"\n",
    paste(get("block_scriptfilesedit", envir=.GlobalEnv),sep="",collapse="\n"),
    paste(modules[[8]],collapse="\n"),"\n",
    sep="",collapse="\n"
  )
  phpfile = get("phpfile", envir=.GlobalEnv)
  fileConn<-file(phpfile, encoding="UTF-8")
  writeLines(script, fileConn)
  close(fileConn)
}
