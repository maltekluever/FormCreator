<?php 
require_once('functions.php');

// config
################ BLOCK: CONFIG ###############
$phpfile = "test.php";
$sql_table = "Test";
$sql_list = "SELECT `ID`,`Textfield`, DATE_FORMAT(`Datefield`,'%d.%m.%Y') AS Datefield, TIME_FORMAT(`Timefield`, '%H:%i') AS Timefield FROM `Test` ORDER BY `ID` ASC ";
$sql_list_array = array(array("ID" => array("ID","10%"), "Textfield" => array("Text","30%"), "Datefield" => array("Date","30%"), "Timefield" => array("Time","30%")));
$title = "Einträge";
$description = "Hier eine Beschreibung";
$header_add = "Eintrag hinzufügen";
$header_edit = "Eintrag bearbeiten";
$msg_add = "Der Eintrag wurde angelegt!";
$msg_edit = "Der Eintrag wurde gespeichert!";
$msg_delete = "Die markierten Einträge wurden gelöscht!";
$msg_noentries = "Es wurden noch keine Einträge eingestellt";
$button_savechanges = "Änderungen speichern";
$button_add = "Hinzufügen";
$button_delete = "Markierte Einträge Löschen";
$delete_dialog_title = "Einträge löschen";
$delete_dialog_content = "Möchten Sie die markierten Einträge wirklich löschen?";
$delete_dialog_yes = "Ja";
$delete_dialog_no = "Nein";
$error_occured = "Es ist ein Fehler aufgetreten!";

################ BLOCK: CONFIG ###############

if(checkiflogged()){
    $info = getuserinfo();

    ################################################################################# SQL: ADD/EDIT #################################################################################
    if(!empty($_POST)){
        $post = form_clean($_POST);
        
        $errorid = array();
        $write = array();
        $files = array();
        // check the fields
        if($_POST['action']=='add' or $_POST['action']=="edit"){
            $msg = array();
            
            ################ BLOCK: CHECKING FIELDS ################
            // CHECK TEXT
            $column = "Textfield"; $label = "LabelTextfeld";
            # if(strlen($post[$column])<=3){ $msg[] = "Die Eingabe für ".$label." muss mindestens 4 Zeichen lang sein."; $errorid[] = $column; }
            # $post[$column] = str_replace(",",".",$post[$column]); if($post[$column]!="" && !is_numeric($post[$column])){ $msg[] = "Die Eingabe für ".$label." muss eine Dezimalzahl sein!"; $errorid[] = $column; }
            $write[$column] = $post[$column];

            // CHECK TEXTAREA
            $column = 'Commentfield'; $label = "LabelComment";
            #if(strlen($post[$column])<=3){ $msg[] = "Die Eingabe für ".$label." muss mindestens 4 Zeichen lang sein."; $errorid[] = $column; }
            $write[$column] = $post[$column];

            // CHECK SELECT
            $column = "Selectfield"; $label = "LabelSelectfield";
            #if($post[$column]==""){ $msg[] = "Bitte treffen Sie eine Auswahl für ".$label."."; $errorid[] = $column; }
            $write[$column] = $post[$column];
            
            // CHECK RADIO
            $column = "Radiofeld"; $label = "Labelradio";
            #if(empty($post[$column])){ $msg[] = "Bitte wählen Sie ein ".$label."."; $errorid[] = $column; }
            $write[$column] = $post[$column];

            // CHECK CHECKBOX
            $array = array("Checkbox1"=>'a',"Checkbox2"=>'b'); $label = "LabelCheckbox"; $sql="SELECT `ID` AS `Column`, `Value` AS `Value` FROM `Test2` ORDER BY `ID`";
            $checked = 0;
            foreach($array as $column=>$value){
                if($post[$column]!=""){ $checked++; }
                $write[$column] = $post[$column];
            }
            if($sql!=""){
                $rso = mysqli_query($con,$sql);
                while($so=mysqli_fetch_array($rso)){
                    if($post[$so['Column']]!=""){ $checked++; }
                    $write[$so['Column']] = $post[$so['Column']];
                }
            }
            #if($checked==0){ $msg[] = "Bitte wählen Sie mind. ein ".$label."."; $errorid[] = preg_replace('/[^a-zA-Z0-9]/', '', $label); }
        
            // CHECK NUMBER
            $column = "Numberfield"; $label = "LabelNumberfield"; 
            # IF REQUIRED:  if(!is_numeric($post[$column])){ $msg[] = "Die Eingabe für ".$label." ist keine Zahl!"; $errorid[] = $column; }
            if($post[$column]!="" && !is_numeric($post[$column])){ $msg[] = "Die Eingabe für ".$label." ist keine Zahl!"; $errorid[] = $column; } 
            $write[$column] = $post[$column];

            // CHECK DATE
            $column = "Datefield"; $label = "LabelDatefield";
            # IF REQUIRED:  if(!checkdate(explode("-",$post[$column])[1],explode("-",$post[$column])[2],explode("-",$post[$column])[0])){ $msg[] = "Das angegebene Datum ist nicht korrekt für ".$label."!"; $errorid[] = $column; }
            if($post[$column]!="" && !checkdate(explode("-",$post[$column])[1],explode("-",$post[$column])[2],explode("-",$post[$column])[0])){ $msg[] = "Das angegebene Datum ist nicht korrekt für ".$label."!"; $errorid[] = $column; }
            $write[$column] = $post[$column];

            // CHECK TIME
            $column = "Timefield"; $label = "LabelTimefield";
            # IF REQUIRED:  if((explode(":",$post[$column])[0]<0 or explode(":",$post[$column])[0]>23 or explode(":",$post[$column])[1]<0 or explode(":",$post[$column])[1]>59)){ $msg[] = "Die angegebene Uhrzeit ist nicht korrekt für ".$label."!"; $errorid[] = $column; }
            if($post[$column]!="" && (explode(":",$post[$column])[0]<0 or explode(":",$post[$column])[0]>23 or explode(":",$post[$column])[1]<0 or explode(":",$post[$column])[1]>59)){ $msg[] = "Die angegebene Uhrzeit ist nicht korrekt für ".$label."!"; $errorid[] = $column; }
            $write[$column] = $post[$column];

            // CHECK EMAIL
            $column = "Emailfield"; $label = "LabelEmailfield";
            if($post[$column]!="" && !form_isemail($post[$column])){ $msg[] = "Die Email-Adresse für ".$label." fehlerhaft"; $errorid[] = $column; }
            $write[$column] = $post[$column];

            // CHECK URL
            $column = "Urlfield"; $label = "LabelUrlfield";
            # IF REQUIRED:  if(!form_isurl($post[$column])){ $msg[] = "Die Internetadresse für ".$label." ist fehlerhaft"; $errorid[] = $column; }
            if($post[$column]!="" && !form_isurl($post[$column])){ $msg[] = "Die Internetadresse für ".$label." ist fehlerhaft"; $errorid[] = $column; }
            if($post[$column]!="http://"){ $write[$column] = $post[$column]; }

            // CHECK FILE
            $column = "Filefield"; $label = "LabelFilefield"; $filetypes = array("jpg","jpeg","gif","png"); $maxfiles=1;
            if(!empty(@array_filter($_FILES[$column]['name']))){
                # check number of files
                if(count($_FILES[$column]['name'])>$maxfiles){ $msg[] = "Es wurden mehr als ".$maxfiles." Dateien ausgewählt!"; $errorid[] = $column; }
                for($x=0;$x<count($_FILES[$column]['name']);$x++){
                    $name = $_FILES[$column]['name'][$x];
                    $temp = $_FILES[$column]['tmp_name'][$x];
                    # check filesize
                    $size = $_FILES[$column]['size'][$x];
                    $max1 = return_bytes(ini_get('post_max_size'));
                    $max2 = return_bytes(ini_get('upload_max_filesize'));
                    if($size > min($max1,$max2)){ $msg[] = "Die Datei ".$name." überschreitet das Upload-Limit von ".format_bytes(min($max1,$max2)); $errorid[] = $column; }
                    # check filetype
                    $type = explode("/",$_FILES[$column]['type'][$x])[1];
                    if(!in_array($type,$filetypes)){ $msg[] = "Die Datei ".$name." hat kein gültiges Dateiformat. Erlaubt sind: ".implode(", ",$filetypes)."."; $errorid[] = $column; }
                    $files[$column][] = $temp;
                }
            }
        
            ################ BLOCK: CHECKING FIELDS ################
            
        }
    
        ################ BLOCK: PROCESS UPLOADED FILES ################
        // HANDLE UPLOADED FILES
        $column = "Filefield";
        if(@count($files[$column])>0){
            # ADD-PROCESS
            if($_POST['action']=="add"){
                for($x=0;$x<count($files[$column]);$x++){
                    $tempfile = $files[$column][$x];                            # local temp file 
                    $extension = explode("/",mime_content_type($tempfile))[1];  # extension
                    $local = $info['temp']."/image.".$extension;                # local file
                    $remote = "pictures/image.".$extension;                     # remote (ftp) file
                    # move_uploaded_file($tempfile,$local);
                    # image_resize($local, 20, 20, true);
                    # $id = mysqli_fetch_array(mysqli_query($con,"SELECT MAX(`ID`) AS `ID` FROM `Test` "))['ID'];
                    # ftp_file_exists("pictures/image.png");
                    # ftp_is_dir("pictures");
                    # ftp_check_connection(); ftp_mkdir($ftp,"pictures");
                    # ftp_upload($local, $remote, true);
                }
            }
            # EDIT-PROCESS
            if($_POST['action']=="edit"){
                for($x=0;$x<count($files[$column]);$x++){
                    $tempfile = $files[$column][$x];                            # local temp file 
                    $extension = explode("/",mime_content_type($tempfile))[1];  # extension
                    $local = $info['temp']."/image.".$extension;                # local file
                    $remote = "pictures/image.".$extension;                     # remote (ftp) file                    
                    # move_uploaded_file($tempfile,$local);
                    # image_resize($local, 20, 20, true);
                    # ftp_file_exists("pictures/image.png");
                    # ftp_is_dir("pictures");
                    # ftp_check_connection(); ftp_mkdir($ftp,"pictures");
                    # ftp_upload($local, $remote, true);
                }                
            }
        }
        
        ################ BLOCK: PROCESS UPLOADED FILES ################
        
        // INSERT THE FIELDS
        if($_POST['action']=='add' && count($msg)==0 && count($write)>0){
            mysqli_query($con,"INSERT INTO `$sql_table` (`".implode("`, `",array_keys($write))."`) VALUES ('".implode("', '",array_values($write))."') ");
            alert('1',array($msg_add));
            $_POST = array();
        }

        // UPDATE THE FIELDS
        if($_POST['action']=='edit' && count($msg)==0 && count($write)>0){
            $update = array();
            foreach($write as $column=>$value){
                $update[] = "`".$column."`='".$value."'";
            }
            $sql = "UPDATE `$sql_table` SET ".implode(", ",$update)." WHERE `ID`='".$post['edit']."'";
            mysqli_query($con,$sql);
            alert('1',array($msg_edit));
            ?>
            <script>
                window.opener.location.href = 'index.php?go=<? echo str_replace(".php","",$phpfile); ?>&reload=<? echo time(); ?>';
            </script>
            <?
            $_POST = array();
            $_POST['edit'] = $post['edit'];
        }

        // ERROR
        if( ($_POST['action']=='add' OR $_POST['action']=='edit') && count($msg)>0){
            alert('2',$msg);
            if($_POST['action']=='add'){
                ?>
                <script>
                    location.hash = '#form';
                </script>
                <?
            }
        }
        
    }    
    
    ################################################################################# SQL: DELETE #################################################################################
    if(!empty($_GET['delete']) && empty($_GET['edit']) && empty($_POST['edit'])){
        $delete = explode("|",$_GET['delete']);
        $deletenr=0;
        for($x=0;$x<count($delete);$x++){
            $deleteid = mysqli_real_escape_string($con, $delete[$x]);
            if(is_numeric($deleteid)){
                $deletenr += mysqli_num_rows(mysqli_query($con,"SELECT `ID` FROM `$sql_table` WHERE `ID`='$deleteid' "));
                mysqli_query($con,"DELETE FROM `$sql_table` WHERE `ID`='$deleteid' ");
            }
        }
        if($deletenr>0){ alert('1',array($msg_delete)); } // only alert message when they really existed (prevent message when refreshing page )
    }    
    ?> 
    
    <!-- ################################################################################# LIST ENTRIES #################################################################################-->
    <? if(empty($_GET['edit']) && empty($_POST['edit'])){?>
        <script>
        function getCheckedItems(chkboxName) {
            var checkboxes = document.getElementsByName(chkboxName);
            var checkboxesChecked = [];
            for (var i=0; i<checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    checkboxesChecked.push(checkboxes[i].value);
                }
            }
            return checkboxesChecked.length > 0 ? checkboxesChecked.join('|') : null;
        }

        function edit(id,w='',h='') {
            if(w==''){ w = document.getElementById('add_form').clientWidth; }
            if(h==''){ h = parseInt(document.getElementById('add_form').clientHeight); }
            PopupCenter('index_minimal.php?go=<? echo str_replace(".php","",$phpfile); ?>&edit='+id, w, h);
        }
        </script>

        <table class="table_content">
        <tr>
        <th><? echo $title; ?></th>
        </tr>

        <? if(!empty($description)){ ?>
            <tr>
            <td style="width:700px;"><? echo htmlspecialchars($description); ?></td>
            </tr>
        <? } ?>

        <tr>
        <td>
            <table class="table_body">    
            <?
            $re = mysqli_query($con,$sql_list);
            if(mysqli_num_rows($re)>0){ ?>
                <tr>
                <? foreach($sql_list_array as $column=>$values){ ?>
                    <th style="width:<? echo $values[1]; ?>;"><b><? echo $values[0]; ?></b></th>
                <? } ?>
                <th></th>
                </tr>
                <?
                while($s=mysqli_fetch_array($re)){ ?>
                    <tr>
                    <? foreach($sql_list_array as $column=>$values){ ?>
                        <td style="width:<? echo $values[1]; ?>" onclick="edit('<? echo $s['ID']; ?>');"><? echo htmlspecialchars($s[$column]); ?></td>
                    <? } ?>
                    <td><input type="checkbox" name="delete" value="<? echo $s['ID']; ?>" id="delete<? echo $s['ID']; ?>"/><label for="delete<? echo $s['ID']; ?>">&nbsp;</label></td>
                    </tr>
                <? } ?>
            <? }else{ ?>
                <tr>
                <td><? echo $msg_noentries; ?></tr>
                </tr>
            <? } ?>
            </table>
        </td>
        </tr>
        
        <tr>
        <td style="text-align:right;">
            <input type="submit" value="<? echo $button_delete; ?>" onclick="dialog('<? echo $delete_dialog_title; ?>','<? echo $delete_dialog_content; ?>','index.php?go=<? echo str_replace(".php","",$phpfile); ?>&delete='+getCheckedItems('delete'),'<? echo $delete_dialog_yes; ?>','<? echo $delete_dialog_no; ?>'); return false;"/>
        </td>
        </tr>


        <!-- ################################################################################# ADD-FORM #################################################################################-->
        <form method="post" action="index.php" enctype="multipart/form-data">
            <input type="hidden" name="go" value="<? echo str_replace(".php","",$phpfile); ?>"/>
            <input type="hidden" name="action" value="add"/>
            
            <tr>
            <td>
                <table class="table_body_add" id="add_form">  
                <tr>
                <th colspan="2"><? echo $header_add; ?></th>
                </tr>
                
                <!-- ################ BLOCK: INPUT FIELDS (ADD) ################ -->
                <!-- // ADD TEXT -->
                <? $column = "Textfield"; $label="LabelTextfeld"; $width="230px;"; $width_td_left="40%"; $width_td_right="60%"; $options=""; ?>
                <tr <? if(@in_array($column,$errorid)){ ?>class="tr_error"<? } ?>>
                <td style="width:<? echo $width_td_left; ?>;"><? echo $label; ?></td>
                <td style="width:<? echo $width_td_right; ?>; text-align: left;">
                    <input type="text" name="<? echo $column; ?>" value="<? echo htmlspecialchars($_POST[$column]); ?>" style="width: <? echo $width; ?>;" <? echo $options; ?>/>
                </td>
                </tr>

                <!-- // ADD TEXTAREA -->
                <? $column = "Commentfield"; $label="LabelComment"; $width="230px;"; $height="100px"; $width_td_left="40%"; $width_td_right="60%"; $options=""; ?>
                <tr <? if(@in_array($column,$errorid)){ ?>class="tr_error"<? } ?>>
                <td style="width:<? echo $width_td_left; ?>;"><? echo $label; ?></td>
                <td style="width:<? echo $width_td_right; ?>; text-align: left;">
                    <textarea name="<? echo $column; ?>" style="width: <? echo $width; ?>; height:<? echo $height; ?>;" <? echo $options; ?>><? echo htmlspecialchars($_POST[$column]); ?></textarea>
                </td>
                </tr>

                <!-- // ADD SELECT -->
                <? $column = "Selectfield"; $label="LabelSelectfield"; $width="230px;"; $width_td_left="40%"; $width_td_right="60%"; $pleasechoose=true; $array = array('value1'=>'label1', 'value2'=>'label2'); $sql="SELECT `ID` AS `Value`, `Value` AS `Label` FROM `Test2` ORDER BY `ID`"; $options=""; ?>
                <tr <? if(@in_array($column,$errorid)){ ?>class="tr_error"<? } ?>>
                <td style="width:<? echo $width_td_left; ?>;"><? echo $label; ?></td>
                <td style="width:<? echo $width_td_left; ?>; text-align: left;">
                    <select name="<? echo $column; ?>" style="width:<? echo $width; ?>;" <? if($pleasechoose==true){ ?>onfocus="if(this.options[0].label == 'Bitte Wählen'){ this.remove(0); }"<? } ?> <? echo $options; ?>>
                        <?
                        if($pleasechoose==true){ ?><option value="">Bitte Wählen</option><? } // make the "please choose"-option
                        foreach($array as $value=>$label){ ?><option value="<? echo $value; ?>" <? if($_POST[$column]==$value){ echo ' selected="selected"'; } ?>><? echo $label; ?></option><? } // make the $array options
                        if($sql!=""){ $rso = mysqli_query($con,$sql); while($so=mysqli_fetch_array($rso)){ ?><option value="<? echo $so['Value']; ?>"<? if($_POST[$column]==$so['Value']){ echo ' selected="selected"'; } ?>><? echo $so['Label']; ?></option><? }} // make the sql-options
                        ?> 
                    </select>
                </td>
                </tr>
                              
                <!-- // ADD RADIO -->
                <? $column = "Radiofeld"; $label="Labelradio"; $width_td_left="40%"; $width_td_right="60%"; $array = array('value1'=>'label1','value2'=>'label2'); $sql="SELECT `ID` AS `Value`, `Value` AS `Label` FROM `Test2` ORDER BY `ID`"; $byrow=false; ?>
                <tr <? if(@in_array('Radiofeld',$errorid)){ ?>class="tr_error"<? } ?>>
                <td style="width:<? echo $width_td_left; ?>;"><? echo $label; ?></td>
                <td style="width:<? echo $width_td_right; ?>; text-align: left;">
                    <?
                    if($byrow==true){ $br=""; }else{ $br="<br/>"; }
                    foreach($array as $value=>$label){ ?><input type="radio" name="<? echo $column; ?>" value="<? echo $value; ?>" id="i<? echo md5($column."_".$value); ?>" <? if($_POST[$column]==$value){ echo 'checked="checked"'; } ?>/><label for="i<? echo md5($column."_".$value); ?>"><? echo $label; ?></label><? echo $br; } // make the $array checkboxes
                    if($sql!=""){ $rso = mysqli_query($con,$sql); while($so=mysqli_fetch_array($rso)){ ?><input type="radio" name="<? echo $column; ?>" value="<? echo $so['Value']; ?>" id="i<? echo md5($column."_".$so['Value']); ?>" <? if($_POST[$column]==$so['Value']){ echo 'checked="checked"'; } ?>/><label for="i<? echo md5($column."_".$so['Value']); ?>"><? echo $so['Label']; ?></label><? echo $br;  }} // make the sql-options
                    ?>
                </td>
                </tr>          
              
                <!-- // ADD CHECKBOX -->
                <? $array = array("Checkbox1"=>'a', "Checkbox2"=>'b'); $sql="SELECT `ID` AS `Column`, `Value` AS `Value` FROM `Test2` ORDER BY `ID`"; $label = "LabelCheckbox";  $width_td_left="40%"; $width_td_right="60%"; $byrow=true; ?>
                <tr <? if(@in_array(preg_replace('/[^a-zA-Z0-9]/', '', $label),$errorid)){ ?>class="tr_error"<? } ?>>
                <td style="width:<? echo $width_td_left; ?>;"><? echo $label; ?></td>
                <td style="width:<? echo $width_td_left; ?>; text-align: left;">
                    <?
                    if($byrow==true){ $br=""; }else{ $br="<br/>"; }
                    foreach($array as $column => $value){ ?><input type="checkbox" name="<? echo $column; ?>" value="<? echo $value; ?>" id="<? echo $column; ?>" <? if($_POST[$column]==$value){ echo 'checked="checked"'; } ?>/><label for="<? echo $column; ?>"><? echo $value; ?></label><? echo $br; } // make the $array checkboxes
                    if($sql!=""){ $rso = mysqli_query($con,$sql); while($so=mysqli_fetch_array($rso)){ ?><input type="checkbox" name="<? echo $so['Column']; ?>" value="<? echo $so['Value']; ?>" id="<? echo $so['Column']; ?>" <? if($_POST[$so['Column']]==$so['Value']){ echo 'checked="checked"'; } ?>/><label for="<? echo $so['Column']; ?>"><? echo $so['Value']; ?></label><? echo $br;  }} // make the sql-options
                    ?>
                </td>
                </tr>

                <!-- // ADD NUMBER -->
                <? $column = "Numberfield"; $label="LabelNumberfield"; $width="auto;"; $width_td_left="40%"; $width_td_right="60%"; $options=""; ?>
                <tr <? if(@in_array($column,$errorid)){ ?>class="tr_error"<? } ?>>
                <td style="width:<? echo $width_td_left; ?>;"><? echo $label; ?></td>
                <td style="width:<? echo $width_td_right; ?>; text-align: left;">
                    <input type="number" name="<? echo $column; ?>" value="<? echo htmlspecialchars($_POST[$column]); ?>" style="width: <? echo $width; ?>;" <? echo $options; ?>/>
                </td>
                </tr>

                <!-- // ADD DATE -->
                <? $column = "Datefield"; $label="LabelDatefield"; $width="auto;"; $width_td_left="40%"; $width_td_right="60%"; $options=""; ?>
                <tr <? if(@in_array($column,$errorid)){ ?>class="tr_error"<? } ?>>
                <td style="width:<? echo $width_td_left; ?>;"><? echo $label; ?></td>
                <td style="width:<? echo $width_td_right; ?>; text-align: left;">
                    <input type="date" name="<? echo $column; ?>" value="<? echo htmlspecialchars($_POST[$column]); ?>" style="width: <? echo $width; ?>;" <? echo $options; ?>/>
                </td>
                </tr>

                <!-- // ADD TIME -->
                <? $column = "Timefield"; $label="LabelTimefield"; $width="auto;"; $width_td_left="40%"; $width_td_right="60%"; $options=""; ?>
                <tr <? if(@in_array($column,$errorid)){ ?>class="tr_error"<? } ?>>
                <td style="width:<? echo $width_td_left; ?>;"><? echo $label; ?></td>
                <td style="width:<? echo $width_td_right; ?>; text-align: left;">
                    <input type="time" name="<? echo $column; ?>" value="<? echo htmlspecialchars($_POST[$column]); ?>" style="width: <? echo $width; ?>;" <? echo $options; ?>/>
                </td>
                </tr>

                <!-- // ADD EMAIL -->
                <? $column = "Emailfield"; $label="LabelEmailfield"; $width="auto;"; $width_td_left="40%"; $width_td_right="60%"; $options=""; ?>
                <tr <? if(@in_array($column,$errorid)){ ?>class="tr_error"<? } ?>>
                <td style="width:<? echo $width_td_left; ?>;"><? echo $label; ?></td>
                <td style="width:<? echo $width_td_right; ?>; text-align: left;">
                    <input type="email" name="<? echo $column; ?>" value="<? echo htmlspecialchars($_POST[$column]); ?>" style="width: <? echo $width; ?>;" <? echo $options; ?>/>
                </td>
                </tr>

                <!-- // ADD URL -->
                <? $column = "Urlfield"; $label="LabelUrlfield"; $width="auto;"; $width_td_left="40%"; $width_td_right="60%"; $options=""; ?>
                <tr <? if(@in_array($column,$errorid)){ ?>class="tr_error"<? } ?>>
                <td style="width:<? echo $width_td_left; ?>;"><? echo $label; ?></td>
                <td style="width:<? echo $width_td_right; ?>; text-align: left;">
                    <input type="url" name="<? echo $column; ?>" value="<? echo htmlspecialchars($_POST[$column]); ?>" style="width: <? echo $width; ?>;" <? echo $options; ?>/>
                </td>
                </tr>
 
                <!-- // ADD FILE -->
                <? $column = "Filefield"; $label="LabelFilefield"; $filetypes = array("jpg","jpeg","gif","png"); $maxfiles=1; $width="250px;"; $width_td_left="40%"; $width_td_right="60%"; $options=""; ?>
                <tr <? if(@in_array($column,$errorid)){ ?>class="tr_error"<? } ?>>
                <td style="width:40%"><? echo $label; ?></td>
                <td style="width:60%; text-align: left;">
                    <input type="file" id="<? echo $column; ?>" accept="<? echo implode(",",preg_filter('/^/', '.', $filetypes)); ?>" name="<? echo $column; ?>[]" style="width:<? echo $width; ?>;" <? if($maxfiles>1){ ?>multiple<? } ?> onchange="<? echo $column; ?>_select();" onmouseover="document.getElementById('fileupload_<? echo $column; ?>').className='fileupload_hover';" onmouseout="document.getElementById('fileupload_<? echo $column; ?>').className='';"/>
                    <div id="fileupload_<? echo $column; ?>" multiple style="width:<? echo $width; ?>;"></div>               
                </td>
                </tr>
                
                <!-- ################ BLOCK: INPUT FIELDS (ADD) ################ -->
                
                <tr>
                <td colspan="2" style="text-align:center;"><input type="submit" value="<? echo $button_add; ?>"/><a name="form" style=="display:absolute;">&nbsp;</a></td>
                </tr>         
                </table>
            </form>
            
            <!-- ################ BLOCK: SCRIPT FILES (ADD) ################ -->
            <!-- // ADD SCRIPT FILE -->
            <? $column = "Filefield"; $filetypes = array("jpg","jpeg","gif","png"); $description = "Bitte wählen Sie eine Datei aus oder schieben Sie eine Datei in dieses Feld! Erlaubt sind folgende Formate: ".implode(", ",$filetypes)."."; ?>
            <script>
            // get the file info of selected files in a input-file-element
            function <? echo $column; ?>_select(){
                input = document.getElementById('<? echo $column; ?>');
                div  = document.getElementById('fileupload_<? echo $column; ?>');
                if(input.files.length == 0){
                    var txt = "<? echo $description; ?>";
                }else{
                    var txt = "<table class=\"fileinfo\"><tr><td><b>Name</b></td><td><b>Größe</b></td></tr>";
                    for (var i = 0; i < input.files.length; i++) {
                        var file = input.files[i];
                        txt += "<tr>";
                        txt += "<td>" + file.name + "</td>";
                        txt += "<td>" + formatbytes(file.size) + "</td>";
                        txt += "</tr>";
                    }
                    txt += "</table>";
                }
                div.innerHTML = txt;
                input.style.height = div.clientHeight+'px';
            }
            <? echo $column; ?>_select();
            </script>
            
            <!-- ################ BLOCK: SCRIPT FILES (ADD) ################ -->
            
        </td>
        </tr>
        </table>
    <? }else{
        # check if ID actually exists
        $edit = mysqli_real_escape_string($con,$_GET['edit'].$_POST['edit']);
        $re = mysqli_query($con,"SELECT * FROM `$sql_table` WHERE `ID`='$edit' ");
        if(mysqli_num_rows($re)==1){ 
            $s = mysqli_fetch_array($re);
            ?>
            <!-- ################################################################################# EDIT-FORM #################################################################################-->
            <form method="post" action="index_minimal.php" enctype="multipart/form-data">
                <input type="hidden" name="edit" value="<? echo $_GET['edit'].$_POST['edit']; ?>"/>
                <input type="hidden" name="go" value="<? echo str_replace(".php","",$phpfile); ?>"/>
                <input type="hidden" name="action" value="edit"/>
                
                <table class="table_body_edit" id="add_form">  
                <tr>
                <th colspan="2"><? echo $header_edit; ?></th>
                </tr>
                
                
                <!-- ################ BLOCK: INPUT FIELDS (EDIT) ################ -->
                <!-- // EDIT TEXT -->
                <? $column = "Textfield"; $label="LabelTextfeld"; $width="230px;"; $width_td_left="40%"; $width_td_right="60%"; $options = ""; ?>
                <tr <? if(@in_array($column,$errorid)){ ?>class="tr_error"<? } ?>>
                <td style="width:<? echo $width_td_left; ?>;"><? echo $label; ?></td>
                <td style="width:<? echo $width_td_right; ?>; text-align: left;">
                    <input type="text" name="<? echo $column; ?>" value="<? if($_POST['action']=="edit"){ echo htmlspecialchars($_POST[$column]); }else{ echo htmlspecialchars($s[$column]); } ?>" style="width: <? echo $width; ?>;" <? echo $options; ?>/>
                </td>
                </tr>

                <!-- // EDIT TEXTAREA -->
                <? $column = "Commentfield"; $label="LabelComment"; $width="230px;"; $height="100px"; $width_td_left="40%"; $width_td_right="60%"; $options = ""; ?>
                <tr <? if(@in_array($column,$errorid)){ ?>class="error"<? } ?>>
                <td style="width:<? echo $width_td_left; ?>;"><? echo $label; ?></td>
                <td style="width:<? echo $width_td_right; ?>; text-align: left;">
                    <textarea name="<? echo $column; ?>" style="width: <? echo $width; ?>; height:<? echo $height; ?>;" <? echo $options; ?>><? if($_POST['action']=="edit"){ echo htmlspecialchars($_POST[$column]); }else{ echo htmlspecialchars($s[$column]); } ?></textarea>
                </td>
                </tr>

                <!-- // EDIT SELECT -->
                <? $column = "Selectfield"; $label="LabelSelectfield"; $width="230px;"; $width_td_left="40%"; $width_td_right="60%"; $pleasechoose=true; $array = array('value1'=>'label1', 'value2'=>'label2'); $sql="SELECT `ID` AS `Value`, `Value` AS `Label` FROM `Test2` ORDER BY `ID`"; $options=""; ?>
                <tr <? if(@in_array($column,$errorid)){ ?>class="tr_error"<? } ?>>
                <td style="width:<? echo $width_td_left; ?>;"><? echo $label; ?></td>
                <td style="width:<? echo $width_td_left; ?>; text-align: left;">
                    <select name="<? echo $column; ?>" style="width:<? echo $width; ?>;" <? if($pleasechoose==true){ ?>onfocus="if(this.options[0].label == 'Bitte Wählen'){ this.remove(0); }"<? } ?> <? echo $options; ?>>
                        <?
                        if($pleasechoose==true){ ?><option value="">Bitte Wählen</option><? } // make the "please choose"-option
                        foreach($array as $value=>$label){ ?><option value="<? echo $value; ?>" <? if(($_POST['action']=="edit" && $_POST[$column]==$value) or ($_POST['action']!="edit" && $s[$column]==$value)){ echo 'selected="selected"'; } ?>><? echo $label; ?></option><? } // make the $array options
                        $rso = mysqli_query($con,$sql); while($so=mysqli_fetch_array($rso)){ ?><option value="<? echo $so['Value']; ?>" <? if(($_POST['action']=="edit" && $_POST[$column]==$so['Value']) or ($_POST['action']!="edit" && $s[$column]==$so['Value'])){ echo 'selected="selected"'; } ?>><? echo $so['Label']; ?></option><? } // make the sql-options
                        ?> 
                    </select>
                </td>
                </tr>
              
                <!-- // EDIT RADIO -->
                <? $column = "Radiofeld"; $label="Labelradio"; $width_td_left="40%"; $width_td_right="60%"; $array = array('value1'=>'label1','value2'=>'label2'); $sql="SELECT `ID` AS `Value`, `Value` AS `Label` FROM `Test2` ORDER BY `ID`"; $byrow=false; ?>
                <tr <? if(@in_array('Radiofeld',$errorid)){ ?>class="tr_error"<? } ?>>
                <td style="width:<? echo $width_td_left; ?>;"><? echo $label; ?></td>
                <td style="width:<? echo $width_td_right; ?>; text-align: left;">
                    <?
                    if($byrow==true){ $br=""; }else{ $br="<br/>"; }
                    foreach($array as $value=>$label){ ?><input type="radio" name="<? echo $column; ?>" value="<? echo $value; ?>" id="i<? echo md5($column."_".$value); ?>" <? if(($_POST['action']=="edit" && $_POST[$column]==$value) or ($_POST['action']!="edit" && $s[$column]==$value)){ echo 'checked="checked"'; } ?>/><label for="i<? echo md5($column."_".$value); ?>"><? echo $label; ?></label><? echo $br; } // make the $array checkboxes
                    if($sql!=""){ $rso = mysqli_query($con,$sql); while($so=mysqli_fetch_array($rso)){ ?><input type="radio" name="<? echo $column; ?>" value="<? echo $so['Value']; ?>" id="i<? echo md5($column."_".$so['Value']); ?>" <? if(($_POST['action']=="edit" && $_POST[$column]==$so['Value']) or ($_POST['action']!="edit" && $s[$column]==$so['Value'])){ echo 'checked="checked"'; } ?>/><label for="i<? echo md5($column."_".$so['Value']); ?>"><? echo $so['Label']; ?></label><? echo $br;  }} // make the sql-options
                    ?>
                </td>
                </tr>              
              
                <!-- // EDIT CHECKBOX -->
                <? $array = array("Checkbox1"=>'a', "Checkbox2"=>'b'); $sql="SELECT `ID` AS `Column`, `Value` AS `Value` FROM `Test2` ORDER BY `ID`"; $label = "LabelCheckbox";  $width_td_left="40%"; $width_td_right="60%"; $byrow=true; ?>
                <tr <? if(@in_array(preg_replace('/[^a-zA-Z0-9]/', '', $label),$errorid)){ ?>class="tr_error"<? } ?>>
                <td style="width:<? echo $width_td_left; ?>;"><? echo $label; ?></td>
                <td style="width:<? echo $width_td_left; ?>; text-align: left;">
                    <?
                    if($byrow==true){ $br=""; }else{ $br="<br/>"; }
                    foreach($array as $column => $value){ ?><input type="checkbox" name="<? echo $column; ?>" value="<? echo $value; ?>" id="<? echo $column; ?>" <? if(($_POST['action']=="edit" && $_POST[$column]==$value) or ($_POST['action']!="edit" && $value==$s[$column])){ echo 'checked="checked"'; } ?>/><label for="<? echo $column; ?>"><? echo $value; ?></label><? echo $br; } // make the $array checkboxes
                    if($sql!=""){ $rso = mysqli_query($con,$sql); while($so=mysqli_fetch_array($rso)){ ?><input type="checkbox" name="<? echo $so['Column']; ?>" value="<? echo $so['Value']; ?>" id="<? echo $so['Column']; ?>" <? if(($_POST['action']=="edit" && $_POST[$so['Column']]==$so['Value']) or ($_POST['action']!="edit" && $s[$so['Column']]==$so['Value'])){ echo 'checked="checked"'; } ?>/><label for="<? echo $so['Column']; ?>"><? echo $so['Value']; ?></label><? echo $br;  }} // make the sql-options
                    ?>
                </td>
                </tr>

                <!-- // EDIT NUMBER -->
                <? $column = "Numberfield"; $label="LabelNumberfield"; $width="auto;"; $width_td_left="40%"; $width_td_right="60%"; $options=""; ?>
                <tr <? if(@in_array($column,$errorid)){ ?>class="tr_error"<? } ?>>
                <td style="width:<? echo $width_td_left; ?>;"><? echo $label; ?></td>
                <td style="width:<? echo $width_td_right; ?>; text-align: left;">
                    <input type="number" name="<? echo $column; ?>" value="<? if($_POST['action']=="edit"){ echo htmlspecialchars($_POST[$column]); }else{ echo htmlspecialchars($s[$column]); } ?>" style="width: <? echo $width; ?>;" <? echo $options; ?>/>
                </td>
                </tr>

                <!-- // EDIT DATE -->
                <? $column = "Datefield"; $label="LabelDatefield"; $width="auto;"; $width_td_left="40%"; $width_td_right="60%"; $options=""; ?>
                <tr <? if(@in_array($column,$errorid)){ ?>class="tr_error"<? } ?>>
                <td style="width:<? echo $width_td_left; ?>;"><? echo $label; ?></td>
                <td style="width:<? echo $width_td_right; ?>; text-align: left;">
                    <input type="date" name="<? echo $column; ?>" value="<? if($_POST['action']=="edit"){ echo htmlspecialchars($_POST[$column]); }else{ echo htmlspecialchars($s[$column]); } ?>" style="width: <? echo $width; ?>;" <? echo $options; ?>/>
                </td>
                </tr>

                <!-- // EDIT TIME -->
                <? $column = "Timefield"; $label="LabelTimefield"; $width="auto;"; $width_td_left="40%"; $width_td_right="60%"; $options=""; ?>
                <tr <? if(@in_array($column,$errorid)){ ?>class="tr_error"<? } ?>>
                <td style="width:<? echo $width_td_left; ?>;"><? echo $label; ?></td>
                <td style="width:<? echo $width_td_right; ?>; text-align: left;">
                    <input type="time" name="<? echo $column; ?>" value="<? if($_POST['action']=="edit"){ echo htmlspecialchars($_POST[$column]); }else{ echo htmlspecialchars($s[$column]); } ?>" style="width: <? echo $width; ?>;" <? echo $options; ?>/>
                </td>
                </tr>

                <!-- // EDIT EMAIL -->
                <? $column = "Emailfield"; $label="LabelEmailfield"; $width="auto;"; $width_td_left="40%"; $width_td_right="60%"; $options=""; ?>
                <tr <? if(@in_array($column,$errorid)){ ?>class="tr_error"<? } ?>>
                <td style="width:<? echo $width_td_left; ?>;"><? echo $label; ?></td>
                <td style="width:<? echo $width_td_right; ?>; text-align: left;">
                    <input type="email" name="<? echo $column; ?>" value="<? if($_POST['action']=="edit"){ echo htmlspecialchars($_POST[$column]); }else{ echo htmlspecialchars($s[$column]); } ?>" style="width: <? echo $width; ?>;" <? echo $options; ?>/>
                </td>
                </tr>

                <!-- // EDIT URL -->
                <? $column = "Urlfield"; $label="LabelUrlfield"; $width="auto;"; $width_td_left="40%"; $width_td_right="60%"; $options=""; ?>
                <tr <? if(@in_array($column,$errorid)){ ?>class="tr_error"<? } ?>>
                <td style="width:<? echo $width_td_left; ?>;"><? echo $label; ?></td>
                <td style="width:<? echo $width_td_right; ?>; text-align: left;">
                    <input type="url" name="<? echo $column; ?>" value="<? if($_POST['action']=="edit"){ echo htmlspecialchars($_POST[$column]); }else{ echo htmlspecialchars($s[$column]); } ?>" style="width: <? echo $width; ?>;" <? echo $options; ?>/>
                </td>
                </tr>

                <!-- // EDIT FILE -->
                <? $column = "Filefield"; $label="LabelFilefield"; $filetypes = array("jpg","jpeg","gif","png"); $maxfiles=1; $width="250px;"; $width_td_left="40%"; $width_td_right="60%"; $options=""; ?>
                <tr <? if(@in_array($column,$errorid)){ ?>class="tr_error"<? } ?>>
                <td style="width:40%"><? echo $label; ?></td>
                <td style="width:60%; text-align: left;">
                    <input type="file" id="<? echo $column; ?>" accept="<? echo implode(",",preg_filter('/^/', '.', $filetypes)); ?>" name="<? echo $column; ?>[]" style="width:<? echo $width; ?>;" <? if($maxfiles>1){ ?>multiple<? } ?> onchange="<? echo $column; ?>_select();" onmouseover="document.getElementById('fileupload_<? echo $column; ?>').className='fileupload_hover';" onmouseout="document.getElementById('fileupload_<? echo $column; ?>').className='';"/>
                    <div id="fileupload_<? echo $column; ?>" multiple style="width:<? echo $width; ?>;"></div>               
                </td>
                </tr>
                
                <!-- ################ BLOCK: INPUT FIELDS (EDIT) ################ -->
                
                <tr>
                <td colspan="2" style="text-align:center;"><input type="submit" value="<? echo htmlspecialchars($button_savechanges); ?>"/><a name="form" style=="display:absolute;">&nbsp;</a></td>
                </tr>         
                </table>
            </form>
            
            <!-- ################ BLOCK: SCRIPT FILES (EDIT) ################ -->
            <!-- // EDIT SCRIPT FILE -->
            <? $column = "Filefield"; $filetypes = array("jpg","jpeg","gif","png"); $description = "Bitte wählen Sie eine Datei aus oder schieben Sie eine Datei in dieses Feld! Erlaubt sind folgende Formate: ".implode(", ",$filetypes); ?>
            <script>
            // get the file info of selected files in a input-file-element
            function <? echo $column; ?>_select(){
                input = document.getElementById('<? echo $column; ?>');
                div  = document.getElementById('fileupload_<? echo $column; ?>');
                if(input.files.length == 0){
                    var txt = "<? echo $description; ?>";
                }else{
                    var txt = "<table class=\"fileinfo\"><tr><td><b>Name</b></td><td><b>Größe</b></td></tr>";
                    for (var i = 0; i < input.files.length; i++) {
                        var file = input.files[i];
                        txt += "<tr>";
                        txt += "<td>" + file.name + "</td>";
                        txt += "<td>" + formatbytes(file.size) + "</td>";
                        txt += "</tr>";
                    }
                    txt += "</table>";
                }
                div.innerHTML = txt;
                input.style.height = div.clientHeight+'px';
            }
            <? echo $column; ?>_select();
            </script>
            
            <!-- ################ BLOCK: SCRIPT FILES (EDIT) ################ -->
            
            <script>
            <? if(empty($_POST)){ ?>WindowFitContent();<? } # works only when the window was created the first time. After that it the height would incrementally increase. ?>
            </script>
        <? }else{ ?>
            <? echo $error_occured; ?>
        <? } ?>
    <? } ?>
<? }else{
  goto_login();
}
?>
